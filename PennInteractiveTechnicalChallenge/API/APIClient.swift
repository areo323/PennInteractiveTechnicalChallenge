//
//  APIClient.swift
//  PennInteractiveTechnicalChallenge
//
//  Created by Arkadiusz Wazny on 12/8/19.
//  Copyright © 2019 ArkadiuszWazny. All rights reserved.
//

import Alamofire
import CodableAlamofire
import Foundation

class APIClient {
    
    private let sessionManager: Alamofire.SessionManager
    private let decoder: JSONDecoder
    private let queue = DispatchQueue(label: "com.PennInteractiveTechnicalChallenge.response-queue", qos: .utility, attributes: [.concurrent])
    
    init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForResource = 30 // seconds
        
        sessionManager = Alamofire.SessionManager(configuration: configuration)
        
        decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
    }
    
    func makeRequest<ResponseItem: Codable>(with router: RouterProtocol, completion: @escaping (Result<BaseResponseEntity<ResponseItem>>) -> ()) {
        let request = self.sessionManager.request(router)
        request
            .validate()
            .responseDecodableObject(queue: self.queue, decoder: self.decoder) { (response: DataResponse<BaseResponseEntity<ResponseItem>>) in
                self.handleResponse(for: response, endpoint: router, completion: completion)
        }
    }
    
    private func handleResponse<ResponseItem: Codable>(for response: DataResponse<BaseResponseEntity<ResponseItem>>, endpoint: RouterProtocol, completion: @escaping (Result<BaseResponseEntity<ResponseItem>>) -> ()) {
        
        let jsonResponse = prettyJSONResponse(from: response.data)
        
        switch response.result {
        case .success(let decodedResponse):
            debugPrint(jsonResponse)
            DispatchQueue.main.async {
                completion(.success(decodedResponse))
            }
        case .failure(let error):
            debugPrint(jsonResponse)
            
            if let apiError = try? self.decoder.decode(ApiError.self, from: response.data ?? Data()) {
                DispatchQueue.main.async {
                    completion(.failure(apiError))
                }
            } else {
                let unknownError = ApiError(errorID: 400, errorMessage: error.localizedDescription, errorName: "error_uknown")
                DispatchQueue.main.async {
                    completion(.failure(unknownError))
                }
            }

        }
    }
    
    private func prettyJSONResponse(from responseData: Data?) -> String {
        var prettyJSONResponse: String = "Cannot convert response data to JSON"
        if
            let rawJSON = try? JSONSerialization.jsonObject(with: responseData ?? Data(), options: .allowFragments) as? [String: Any] {
            let jsonString = rawJSON.filter({ $0.key != "debug" }).prettyPrint()
            prettyJSONResponse = jsonString
        }
        
        return prettyJSONResponse
    }
}
