//
//  APIError.swift
//  PennInteractiveTechnicalChallenge
//
//  Created by Arkadiusz Wazny on 12/8/19.
//  Copyright © 2019 ArkadiuszWazny. All rights reserved.
//

import Foundation

struct ApiError: Error, Codable {
    let errorID: Int
    let errorMessage, errorName: String
    
    var localizedDescription: String {
        return self.errorMessage
    }

    enum CodingKeys: String, CodingKey {
        case errorID = "error_id"
        case errorMessage = "error_message"
        case errorName = "error_name"
    }
}
