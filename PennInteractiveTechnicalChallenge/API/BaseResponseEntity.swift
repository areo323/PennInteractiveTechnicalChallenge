//
//  BaseResponseEntity.swift
//  PennInteractiveTechnicalChallenge
//
//  Created by Arkadiusz Wazny on 12/8/19.
//  Copyright © 2019 ArkadiuszWazny. All rights reserved.
//

import Foundation

struct BaseResponseEntity<Item: Codable>: Codable {
    let items: [Item]
    let hasMore: Bool
    let quotaMax, quotaRemaining: Int

    enum CodingKeys: String, CodingKey {
        case items
        case hasMore = "has_more"
        case quotaMax = "quota_max"
        case quotaRemaining = "quota_remaining"
    }
}
