//
//  RouterProtocol.swift
//  PennInteractiveTechnicalChallenge
//
//  Created by Arkadiusz Wazny on 12/8/19.
//  Copyright © 2019 ArkadiuszWazny. All rights reserved.
//

import Alamofire
import Foundation

public protocol RouterProtocol: URLRequestConvertible {
    var method: HTTPMethod { get }
    var path: String { get }
    var queryParameters: Parameters { get }
    var bodyParameters: Parameters { get }
    var cached: Bool { get }
}

public extension RouterProtocol {
    
    var apiURL: String {
        return "https://api.stackexchange.com/2.2"
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try apiURL.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        urlRequest.cachePolicy = cached ? .returnCacheDataElseLoad : .reloadIgnoringCacheData
        
        if queryParameters.isNotEmpty {
            urlRequest = try URLEncoding(destination: .queryString).encode(urlRequest, with: queryParameters)
        }
        
        if bodyParameters.isNotEmpty {
            urlRequest = try JSONEncoding.prettyPrinted.encode(urlRequest, with: bodyParameters)
        }
        
        return urlRequest
    }
    
}
