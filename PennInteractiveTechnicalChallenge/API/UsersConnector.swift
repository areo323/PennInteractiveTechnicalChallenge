//
//  UsersConnector.swift
//  PennInteractiveTechnicalChallenge
//
//  Created by Arkadiusz Wazny on 12/8/19.
//  Copyright © 2019 ArkadiuszWazny. All rights reserved.
//

import Alamofire
import Foundation

class UsersConnector {
    
    private let client: APIClient
    
    required init(client: APIClient) {
        self.client = client
    }
    
    func getUsers(_ completion: @escaping (Result<BaseResponseEntity<UserEntity>>) -> ()) {
        let router = UsersRouter.getAllUsers
        client.makeRequest(with: router, completion: completion)
    }
}
