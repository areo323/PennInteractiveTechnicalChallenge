//
//  UsersRouter.swift
//  PennInteractiveTechnicalChallenge
//
//  Created by Arkadiusz Wazny on 12/8/19.
//  Copyright © 2019 ArkadiuszWazny. All rights reserved.
//

import Alamofire
import Foundation

enum UsersRouter: RouterProtocol {
    case getAllUsers

    var method: HTTPMethod {
        switch self {
        case .getAllUsers: return .get
        }
    }
    
    var path: String {
        switch self {
        case .getAllUsers: return "/users"
        }
    }
    
    var queryParameters: Parameters {
        switch self {
        case .getAllUsers: return ["site" : "stackoverflow"]
        }
    }
    
    var bodyParameters: Parameters {
        switch self {
        case .getAllUsers: return [:]
        }
    }
    
    var cached: Bool {
        return true
    }
}
