//
//  NSObject+Extension.swift
//  PennInteractiveTechnicalChallenge
//
//  Created by Arkadiusz Wazny on 12/8/19.
//  Copyright © 2019 ArkadiuszWazny. All rights reserved.
//

import Foundation

extension NSObject {
    
    open class var identifier: String {
        return String(describing: self)
    }
    
    var identifier: String {
        return String(describing: type(of: self))
    }
}
