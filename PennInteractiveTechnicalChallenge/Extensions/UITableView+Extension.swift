//
//  UITableView+Extension.swift
//  PennInteractiveTechnicalChallenge
//
//  Created by Arkadiusz Wazny on 12/8/19.
//  Copyright © 2019 ArkadiuszWazny. All rights reserved.
//

import UIKit

// MARK: Cell
extension UITableView {
    func dequeueReusableCell<T: UITableViewCell>(withClass: T.Type, for indexPath: IndexPath) -> T {
        return dequeueReusableCell(withIdentifier: T.identifier, for: indexPath) as? T ?? T(style: .default, reuseIdentifier: T.identifier)
    }
    
    func registerNib(for className: String) {
        let nib = UINib(nibName: className, bundle: nil)
        register(nib, forCellReuseIdentifier: className)
    }
}

// MARK: Refresh control
extension UITableView {
    
    func addRefreshControl(target: Any, selector: Selector) {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(target, action: selector, for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.gray
        self.refreshControl = refreshControl

    }
    
    func endRefreshing() {
        refreshControl?.endRefreshing()
    }
    
    func startRefreshing() {
        refreshControl?.beginRefreshing()
    }
    
}
