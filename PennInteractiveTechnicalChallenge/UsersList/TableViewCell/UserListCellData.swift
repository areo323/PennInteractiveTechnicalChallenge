//
//  UserListCellModel.swift
//  PennInteractiveTechnicalChallenge
//
//  Created by Arkadiusz Wazny on 12/8/19.
//  Copyright © 2019 ArkadiuszWazny. All rights reserved.
//

import Foundation

struct UserListCellData {
    let badgeCounts: BadgeCounts
    let profileImage: String
    let displayName: String
    
    init(user: UserEntity) {
        self.badgeCounts = user.badgeCounts
        self.displayName = user.displayName
        self.profileImage = user.profileImage
    }
}
