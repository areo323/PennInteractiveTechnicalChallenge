//
//  UsersListTableViewCell.swift
//  PennInteractiveTechnicalChallenge
//
//  Created by Arkadiusz Wazny on 12/8/19.
//  Copyright © 2019 ArkadiuszWazny. All rights reserved.
//

import SDWebImage
import UIKit

class UsersListTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var userAvatarImageView: UIImageView!
    @IBOutlet private weak var userNameLabel: UILabel!
    @IBOutlet private weak var goldBadgeNumberLabel: UILabel!
    @IBOutlet private weak var silverBadgeNumberLabel: UILabel!
    @IBOutlet private weak var bronzeBadgeNumberLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        userAvatarImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
    }
    
    func configureWith(_ cellModel: UserListCellData) {
        setupAvatarImagefor(url: cellModel.profileImage)
        userNameLabel.text = cellModel.displayName
        goldBadgeNumberLabel.text = String(cellModel.badgeCounts.gold)
        silverBadgeNumberLabel.text = String(cellModel.badgeCounts.silver)
        bronzeBadgeNumberLabel.text = String(cellModel.badgeCounts.bronze)
    }
    
    private func setupAvatarImagefor(url: String) {
        let url = URL(string: url)
        userAvatarImageView.sd_setImage(
            with: url,
            placeholderImage: nil,
            options: .continueInBackground,
            context: nil,
            progress: nil,
            completed: nil
        )
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        userAvatarImageView.image = nil
        userNameLabel.text = nil
        goldBadgeNumberLabel.text = nil
        silverBadgeNumberLabel.text = nil
        bronzeBadgeNumberLabel.text = nil
    }
}
