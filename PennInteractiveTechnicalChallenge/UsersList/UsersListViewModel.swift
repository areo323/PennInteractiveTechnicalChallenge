//
//  UsersListViewModel.swift
//  PennInteractiveTechnicalChallenge
//
//  Created by Arkadiusz Wazny on 12/8/19.
//  Copyright © 2019 ArkadiuszWazny. All rights reserved.
//

import Foundation

protocol UsersListViewModelDelegate: class {
    func viewModelDidFinishFetchingUsersWithSuccess()
    func viewModelDidFinishFetchingUsersWithError(_ error: Error)
}

class UsersListViewModel {
    
    private let connector: UsersConnector
    private(set) var users: [UserEntity] = []
    
    weak var delegate: UsersListViewModelDelegate?
    
    init(connector: UsersConnector) {
        self.connector = connector
    }
    
    func getUsers() {
        connector.getUsers { [weak self] result in
            switch result {
            case let .success(response):
                self?.users = response.items
                self?.delegate?.viewModelDidFinishFetchingUsersWithSuccess()
            case let .failure(error):
                self?.delegate?.viewModelDidFinishFetchingUsersWithError(error)
            }
        }
    }
    
    func cellModel(for indexPath: IndexPath) -> UserListCellData {
        return UserListCellData(user: users[indexPath.row])
    }
}
