//
//  UsersListViewController.swift
//  PennInteractiveTechnicalChallenge
//
//  Created by Arkadiusz Wazny on 12/8/19.
//  Copyright © 2019 ArkadiuszWazny. All rights reserved.
//

import UIKit

class UsersListViewController: UIViewController {
    @IBOutlet private weak var tableView: UITableView!
    
    private var viewModel: UsersListViewModel
    
    init(viewModel: UsersListViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Users List"
        setupTableView()
        viewModel.delegate = self
        
        reloadData()
        
    }
    
    private func setupTableView() {
        tableView.dataSource = self
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableView.automaticDimension
        tableView.addRefreshControl(target: self, selector: #selector(reloadData))
        tableView.tableFooterView = UIView()
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.registerNib(for: UsersListTableViewCell.identifier)
    }
    
    @objc
    private func reloadData() {
        tableView.startRefreshing()
        viewModel.getUsers()
    }
    
    private func presentAlertWith(message: String) {
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
                
        let tryAgainAction = UIAlertAction(title: "Try Again ", style: .default) { [weak self] _ in
            self?.reloadData()
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)

        alertController.addAction(tryAgainAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
}

extension UsersListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: UsersListTableViewCell.self, for: indexPath)
        let cellData = viewModel.cellModel(for: indexPath)
        cell.configureWith(cellData)
        return cell
    }
}

extension UsersListViewController: UsersListViewModelDelegate {
    func viewModelDidFinishFetchingUsersWithSuccess() {
        tableView.endRefreshing()
        tableView.reloadData()
    }
    
    func viewModelDidFinishFetchingUsersWithError(_ error: Error) {
        tableView.endRefreshing()
        presentAlertWith(message: error.localizedDescription)
        
    }
}
