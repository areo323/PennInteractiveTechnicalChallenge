## PennInteractiveTechnicalChallange

This repository contains demo Xcode project which fetch first page of users from StackOverflow and display it in tableview.

**Important:**  This app will run only on simulator. To run it on device you need to sign it with your own signing certificate and change bundle identifier

### Installation

In order to build or run project you need to install pods using this command

```
pod install
```

If you don't have CocoaPods installed on your mac here you will find how to do it. [Install CocoaPods](https://cocoapods.org)

